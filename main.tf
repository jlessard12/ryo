provider "aws" {
  profile = "default"
  region  = "us-east-1"
}



resource "aws_instance" "testInstance1" {
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  key_name               = "key_one"
  user_data = "${file("install_docker.sh")}"
  tags = {
    Name = "Worker"
  }
}

